from flask import Flask
from flask_restx import Api, Resource

from controllerContacto import ControllerContacto
app = Flask(__name__)
api = Api(app)

api.add_resource(ControllerContacto, '/contactos')

if __name__ == '__main__':
  app.run(port=8000, debug=True)
