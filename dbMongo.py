import pymongo

class Conexion:
    def __init__(self):
        myclient = pymongo.MongoClient("mongodb://localhost27017/")
        mydb = myclient["agendatelefonica"]
        mycol = mydb["py"]
    
    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()

    def agragarVarios(self, objList):
        x=self.cursor.insert_many(objList)
        print(x.insert_ids)

    def traerLista(self):
        for x in self.cursor.find():
            print(x)

    def traerData(self,myquery):
        mydoc = self.cursor.find(myquery)
        for x in mydoc:
            print(x)

    def actualizarData(self,myquery,newvalues):
        x= self.cursor.update_one(myquery,newvalues)
        print(x.modified_count, "doc actualizado")

    def borrarData(self,myquery):
        x= self.cursor.delete_many(myquery)
        print(x.delete_count, "doc borrado")
