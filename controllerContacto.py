from flask_restx import Api, Resource
from dbMongo import Conexion

conexion = Conexion()

#definiendo la estrucura del Crud
class ControllerContacto(Resource):

    def post(self):
        return 'post'

    def get(self):
        return 'get'

    def put(self):
        return 'put'

    def delete(self):
        return 'delete'